# Gitlab-CI and docker

Example of how to use `GitLab-CI/CD`, `docker`, and `docker-compose`

See [this blog post](https://vgarcia.dev/blog/2019-06-17-building-docker-images-using-docker-compose-and-gitlab/)
for a more comprehensive explanation 
